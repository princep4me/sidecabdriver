import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMessage = '';

  constructor(
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    console.log(form.value);
    if (!form.valid) {
      return;
    }
    this.authService.login(form.value).subscribe(
      (res) => {
        console.log(res);
        this.errorMessage = '';
        if (res['success']) {
          localStorage.setItem('authToken', res['data'].token);
          this.router.navigate(['/dashboard']);
        }
      },
      (err) => {
        console.log(err.error);
        this.errorMessage = err.error.message;
        console.log(this.errorMessage);
      }
    );
  }

}
