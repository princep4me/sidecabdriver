import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PassengerDetailComponent } from './components/passenger-detail/passenger-detail.component';
import { PassengerListComponent } from './components/passenger-list/passenger-list.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Passengers'
        },
        children: [
            {
                path: ':id',
                component: PassengerDetailComponent,
                data: {
                    title: 'Passenger Details'
                }
            },
            {
                path: '',
                component: PassengerListComponent,
                data: {
                    title: 'Passenger List'
                }
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PassengerRoutingModule { }
