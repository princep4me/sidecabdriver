import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PassengerRoutingModule } from './passenger-routing.module';
import { FormsModule } from '@angular/forms';

import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ModalModule } from 'ngx-bootstrap/modal';

import { PassengerListComponent } from './components/passenger-list/passenger-list.component';
import { PassengerDetailComponent } from './components/passenger-detail/passenger-detail.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [PassengerListComponent, PassengerDetailComponent],
  imports: [
    CommonModule,
    FormsModule,
    PassengerRoutingModule,
    SharedModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot()
  ]
})
export class PassengersModule { }
