import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { debounceTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PassengerService {

  apiUrl = environment.apiUrl ;

  constructor(private http: HttpClient) { }

  getPassengers(paginationAndFilterData) {
    return this.http.post(this.apiUrl + 'passengers', paginationAndFilterData).pipe(debounceTime(2000));
  }

  getPassenger(passengerId) {
    return this.http.get(this.apiUrl + 'passengers/' + passengerId);
  }

  deletePassenger(passengerId) {
    return this.http.delete(this.apiUrl + 'passengers/' + passengerId);
  }

  updatePassengerDetails(passengerId, data) {
    return this.http.put(this.apiUrl + 'passengers/' + passengerId, data);
  }
}
