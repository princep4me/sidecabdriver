import { Component, OnInit, ViewChild } from '@angular/core';
import { PassengerService } from '../../services/passenger.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { environment } from 'src/environments/environment';
import { UtilsService } from 'src/app/modules/shared/services/utils.service';
import { StatusConfirmModalComponent } from 'src/app/modules/shared/components/status-confirm-modal/status-confirm-modal.component';
import { EditModalComponent } from 'src/app/modules/shared/components/edit-modal/edit-modal.component';

@Component({
  selector: 'app-passenger-detail',
  templateUrl: './passenger-detail.component.html',
  styleUrls: ['./passenger-detail.component.scss']
})
export class PassengerDetailComponent implements OnInit {

  passenger;
  passengerId: number;
  passengerStatus = false;
  uploadUrl = environment.uploadUrl ;
  editData;
  @ViewChild('confirmModal') confirmModal: StatusConfirmModalComponent;
  @ViewChild('editModal') editModal: EditModalComponent;

  constructor(private passengerService: PassengerService, private route: ActivatedRoute, private router: Router, public utilService: UtilsService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.passengerId = params['id'];
      this.getPassenger();
    });

  }

  getPassenger() {
    this.passengerService.getPassenger(this.passengerId).subscribe(
      (res) => {
        this.passenger = res['data'];
        console.log(this.passenger);
        this.editData = {
          contact_number: this.passenger.contact_number,
          email: this.passenger.email,
          name: {
            first: this.passenger.name.first,
            last: this.passenger.name.last
          }
        };
        this.passengerStatus = this.passenger.status === 1 ? true : false;
      }
    );
  }

  changeStatus(e) {
    this.getPassenger();
  }

  changePassengerDetails() {
    this.passengerService.updatePassengerDetails(this.passengerId, this.editData).subscribe(
      (res) => {
        this.getPassenger();
      }
    );

  }

  goToPassengerList() {
    this.router.navigate(['/customers']);
  }

  showConfirmModal() {
    this.confirmModal.showModal(this.passenger, 3);
  }

  showEditModal() {
    let editDetails = JSON.parse(JSON.stringify(this.passenger))
    this.editModal.showModal(editDetails, 3);
  }

  updated(e){
    console.log("updated");
    this.getPassenger();
  }

}
