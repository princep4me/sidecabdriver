import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { PassengerService } from '../../services/passenger.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UtilsService } from 'src/app/modules/shared/services/utils.service';
import { StatusConfirmModalComponent } from 'src/app/modules/shared/components/status-confirm-modal/status-confirm-modal.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { EditModalComponent } from 'src/app/modules/shared/components/edit-modal/edit-modal.component';
import { DeleteModalComponent } from 'src/app/modules/shared/components/delete-modal/delete-modal.component';
declare var $: any;

@Component({
  selector: 'app-passenger-list',
  templateUrl: './passenger-list.component.html',
  styleUrls: ['./passenger-list.component.scss']
})
export class PassengerListComponent implements OnInit {

  totalPassengerCount = 10;
  activePassengerCount = 0;
  paginationAndFilter = {
    perPage: 10,
    pageNo: 1,
    search: '',
    status: ''
  };
  totalCount = 0;
  disableRadio = true;
  passengers = [];
  currentPassenger: { _id: any; };
  uploadUrl = environment.uploadUrl ;
  @ViewChild('confirmModal') confirmModal: StatusConfirmModalComponent;
  @ViewChild('editModal') editModal: EditModalComponent;
  @ViewChild('deleteModal') deleteModal: DeleteModalComponent;
  errorMessage:string='';
  editData: any;

  constructor(private passengerService: PassengerService, private router: Router,
    public utilService: UtilsService,private modalService:BsModalService) { }

  ngOnInit() {
    this.getPassengers();

    // $('#confirmModal').on('hidden.bs.modal', () => {
    //   console.log('modal close');
    //   this.getPassengers();
    // });
  }

  getPassengers() {
    this.passengerService.getPassengers(this.paginationAndFilter).subscribe(
      (res) => {
        console.log(res);
        this.passengers = res['data'].passengers;
        // console.log(this.passengers);
        this.totalPassengerCount = res['data'].count_data.totalPassengerCount;
        this.activePassengerCount = res['data'].count_data.activePassengerCount;

        this.totalCount = res['data'].paginationVariables.total_count;
      }
    );
  }

  openDeleteModal(passenger: any) {
    this.deleteModal.showModal(passenger, 3);
  }

  changeStatus(e: any) {
    this.getPassengers();
  }

  showConfirmModal(passenger: any, status: any) {
    this.confirmModal.showModal(passenger, 3);
  }

  onCancel() {
    this.getPassengers();
  }

  viewPassenger(passenger: { _id: string; }) {
    this.router.navigate(['customers/' + passenger._id]);
  }

  onPerPageChanged() {
    this.paginationAndFilter.pageNo = 1;
    this.paginationAndFilter.perPage = Number(this.paginationAndFilter.perPage);
    console.log(this.paginationAndFilter.perPage);
    this.getPassengers();
  }

  onStatusChanges() {
    this.paginationAndFilter.pageNo = 1;
    // this.paginationAndFilter.status = Number(this.paginationAndFilter.status);
    console.log(this.paginationAndFilter.status);
    this.getPassengers();
  }

  pageChanged(e: { page: number; }) {
    this.paginationAndFilter.pageNo = e.page;
    this.getPassengers();
  }

  getProfilePicture(passenger: { picture: string; }) {
    if(passenger.picture) {
      return 'url(' + this.uploadUrl + passenger.picture + ')'
    } else {
      return 'url(../../../../../assets/img/profile.jpg )'
    }
  }

  openEditModal(index:number){
    this.editData=JSON.parse(JSON.stringify(this.passengers[index]));
    console.log(this.editData);
    this.editModal.showModal(this.editData,3);
  }

  updated(e){
    console.log("updated");
    this.getPassengers();
  }

  deleted(e){
    console.log("deleted");
    this.getPassengers();
  }
}
