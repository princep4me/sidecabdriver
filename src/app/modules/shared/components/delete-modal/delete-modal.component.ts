import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { PassengerService } from 'src/app/modules/passengers/services/passenger.service';
import { DriverService } from 'src/app/modules/drivers/services/driver.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {
  @ViewChild ('modal') modal:any;
  @Output() deleteEvent= new EventEmitter();
  userType=2;
  user:any;
  constructor(private passengerService:PassengerService,private driverService:DriverService) { }

  ngOnInit() {
  }

  showModal(data:any,type:number){
    console.log(data);
    this.user=data;
    this.userType=type;
    this.modal.show();
  }

  deletePassenger() {
    if (this.userType==2) {
      this.driverService.deleteDriver(this.user._id).subscribe(
        (res) => {
          this.deleteEvent.emit();
          this.modal.hide();
        },
        (err)=>console.log(err)
      );
    } else {
      this.passengerService.deletePassenger(this.user._id).subscribe(
        (res) => {
          this.deleteEvent.emit();
          this.modal.hide();
        },
        (err)=>console.log(err)
      );
    }
  }
}
