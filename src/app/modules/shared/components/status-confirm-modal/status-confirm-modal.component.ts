import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { PassengerService } from 'src/app/modules/passengers/services/passenger.service';
import { DriverService } from 'src/app/modules/drivers/services/driver.service';
import { EventEmitter } from '@angular/core';


@Component({
  selector: 'app-status-confirm-modal',
  templateUrl: './status-confirm-modal.component.html',
  styleUrls: ['./status-confirm-modal.component.scss']
})
export class StatusConfirmModalComponent implements OnInit {

  @ViewChild('myModal') modal;
  userStatus = false;
  user;
  userType = 2;
  @Output() statusChangeSuccess = new EventEmitter();
  @Output() cancelModal = new EventEmitter();

  constructor(private passengerService: PassengerService, private driverService: DriverService) { }

  ngOnInit() {
  }

  showModal(user, userType) {
    this.user = user;
    this.userType = userType;
    this.userStatus = this.user.status === 1 ? true : false;
    this.modal.show();
  }

  hideModal() {
    this.modal.hide();
  }

  handler(event) {
    this.cancelModal.emit();
  }

  changeStatus() {
    const status = this.user.status === 1 ? 2 : 1;

    if(this.userType === 2) {
      this.driverService.updateDriverDetails(this.user._id, {status}).subscribe(
        (res) => {
          if (res['success']) {
            this.statusChangeSuccess.emit();
            console.log('successfully update driver');
            this.hideModal();
          }
        }
      )
    }

    if(this.userType === 3) {
      this.passengerService.updatePassengerDetails(this.user._id, {status}).subscribe(
        (res) => {
          if (res['success']) {
            this.statusChangeSuccess.emit('');
            console.log('successfully update passenger');
            this.hideModal();
          }
        }
      );
    }
  }

}
