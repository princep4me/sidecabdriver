import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusConfirmModalComponent } from './status-confirm-modal.component';

describe('StatusConfirmModalComponent', () => {
  let component: StatusConfirmModalComponent;
  let fixture: ComponentFixture<StatusConfirmModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusConfirmModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusConfirmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
