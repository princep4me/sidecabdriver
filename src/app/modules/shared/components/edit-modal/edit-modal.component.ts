import { Component, OnInit,ViewChild, Output, EventEmitter } from '@angular/core';
import { PassengerService } from 'src/app/modules/passengers/services/passenger.service';
import { NgForm } from '@angular/forms';
import { DriverService } from 'src/app/modules/drivers/services/driver.service';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss']
})
export class EditModalComponent implements OnInit {
  @ViewChild('modal') modal: any;
  editData ={
    name: {
      first: '',
      last: ''
    },
    email: '',
    contact_number: 0,
    _id: ''
  };
  userType = 2;
  @Output() updateEvent=new EventEmitter();
  errorMessage: string='';

  constructor(private passengerService:PassengerService,private driverService:DriverService) { }

  ngOnInit() {
  }

  showModal(user: any, userType: number) {
    this.editData = user;
    this.userType = userType;
    this.modal.show();
  }
  
  updatePassengerDetails(form:NgForm) {
    if (!form.valid) return;
    let _id=this.editData._id;
    delete this.editData._id;
    if (this.userType==3) {
      this.passengerService.updatePassengerDetails(_id, this.editData).subscribe(
        (res: any) => {
          this.modal.hide();
          this.updateEvent.emit();
        },(err: { error: { message: any; }; })=>{
          console.log(err);
          this.errorMessage = err.error.message;
        });
    } else if(this.userType == 2) {
      this.driverService.updateDriverDetails(_id, this.editData).subscribe(
        (res: any) => {
          this.modal.hide();
          this.updateEvent.emit();
        },(err: { error: { message: any; }; })=>{
          console.log(err);
          this.errorMessage = err.error.message;
        });
    }
  }
}
