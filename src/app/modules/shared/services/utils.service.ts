import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  uploadUrl = environment.uploadUrl ;
  
  constructor() { }

  getProfilePicture(user) {
    if(user.picture) {
      return 'url(' + this.uploadUrl + user.picture + ')'
    } else {
      return 'url(../../../../assets/img/User_Avatar.png )'
    }
  }
}
