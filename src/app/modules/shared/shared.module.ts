import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusConfirmModalComponent } from './components/status-confirm-modal/status-confirm-modal.component';

import { ModalModule } from 'ngx-bootstrap/modal';
import { EditModalComponent } from './components/edit-modal/edit-modal.component';
import { FormsModule } from '@angular/forms';
import { DeleteModalComponent } from './components/delete-modal/delete-modal.component';

@NgModule({
  declarations: [StatusConfirmModalComponent, EditModalComponent, DeleteModalComponent],
  imports: [
    CommonModule,
    ModalModule,
    FormsModule,
  ],
  exports: [
    StatusConfirmModalComponent,EditModalComponent,DeleteModalComponent
  ]
})
export class SharedModule { }
