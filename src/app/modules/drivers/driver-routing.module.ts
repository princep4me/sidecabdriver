import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriverDetailComponent } from './components/driver-detail/driver-detail.component';
import { DriverListComponent } from './components/driver-list/driver-list.component';

const routes: Routes = [
  {
      path: '',
      data: {
          title: 'Drivers'
      },
      children: [
          {
              path: ':id',
              component: DriverDetailComponent,
              data: {
                  title: 'Driver Details'
              }
          },
          {
              path: '',
              component: DriverListComponent,
              data: {
                  title: 'Driver List'
              }
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DriverRoutingModule { }
