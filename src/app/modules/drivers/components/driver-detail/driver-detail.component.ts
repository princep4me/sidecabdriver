import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DriverService } from '../../services/driver.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import {ConfirmModalComponent} from '../modals/confirm-modal/confirm-modal.component';
import { NgForm } from '@angular/forms';
import { UtilsService } from 'src/app/modules/shared/services/utils.service';
import { StatusConfirmModalComponent } from 'src/app/modules/shared/components/status-confirm-modal/status-confirm-modal.component';
import { EditModalComponent } from 'src/app/modules/shared/components/edit-modal/edit-modal.component';

@Component({
  selector: 'app-driver-detail',
  templateUrl: './driver-detail.component.html',
  styleUrls: ['./driver-detail.component.scss']
})
export class DriverDetailComponent implements OnInit {
  // @ViewChild ('confirm') confirmModal:BsModalRef;
  driver;
  driverId: number;
  driverStatus = false;
  editData:any={};
  @ViewChild('confirmModal') confirmModal: StatusConfirmModalComponent;
  @ViewChild('editModal') editModal: EditModalComponent;

  errorMessage:string='';

  constructor(private driverService: DriverService,private route:ActivatedRoute,
    private modalService:BsModalService,private router:Router, public utilService: UtilsService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.driverId = params['id'];
      this.getDriver();
    });
  }

  getDriver() {
    this.driverService.getDriver(this.driverId).subscribe(
      (res) => {
        this.driver = res['data'];
        console.log(JSON.stringify(this.driver));
        this.editData = {
          contact_number: this.driver.contact_number,
          email: this.driver.email,
        };
        this.driverStatus = this.driver.status === 1 ? true : false;
      }
    );
  }

  back() {
    this.router.navigate(['/customers']);
  }

  showEditModal() {
    let editDetails = JSON.parse(JSON.stringify(this.driver))
    this.editModal.showModal(editDetails, 2);
  }

  showConfirmModal() {
    this.confirmModal.showModal(this.driver, 2);
  }

  changeStatus(e) {
    this.getDriver();
  }

  updated(e){
    console.log("updated");
    this.getDriver();
  }
}