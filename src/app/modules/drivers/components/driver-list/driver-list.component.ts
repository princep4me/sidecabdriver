import { Component, OnInit, ViewChild } from '@angular/core';
import { DriverService } from '../../services/driver.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/modules/shared/services/utils.service';
import { EditModalComponent } from 'src/app/modules/shared/components/edit-modal/edit-modal.component';
import { DeleteModalComponent } from 'src/app/modules/shared/components/delete-modal/delete-modal.component';

@Component({
  selector: 'app-driver-list',
  templateUrl: './driver-list.component.html',
  styleUrls: ['../../../passengers/components/passenger-list/passenger-list.component.scss']
})
export class DriverListComponent implements OnInit {

  totalDriverCount = 10;
  activeDriverCount = 0;
  inactiveDriverCount = 0;
  paginationAndFilter = {
    perPage: 10,
    pageNo: 1,
    search: '',
    status: ''
  };
  disableRadio = true;
  drivers = [];
  currentDriver;
  uploadUrl = environment.uploadUrl ;
  @ViewChild('editModal') editModal: EditModalComponent;
  @ViewChild('deleteModal') deleteModal: DeleteModalComponent;

  constructor(private driverService: DriverService, private router: Router, private utilService: UtilsService) { }

  ngOnInit() {
    this.getDrivers();
  }

  getDrivers() {
    this.driverService.getDrivers(this.paginationAndFilter).subscribe(
      (res) => {
        console.log(res);
        this.drivers = res['data'].drivers;
        this.totalDriverCount = res['data'].count_data.totalDriverCount;
        this.activeDriverCount = res['data'].count_data.activeDriverCount;
        this.inactiveDriverCount = this.totalDriverCount - this.activeDriverCount;
      }
    );
  }

  viewDriver(driver) {
    this.router.navigate(['drivers/' + driver._id]);
  }

  onPerPageChanged() {
    this.paginationAndFilter.pageNo = 1;
    this.paginationAndFilter.perPage = Number(this.paginationAndFilter.perPage);
    console.log(this.paginationAndFilter.perPage);
    this.getDrivers();
  }

  onStatusChanges() {
    this.paginationAndFilter.pageNo = 1;
    // this.paginationAndFilter.status = Number(this.paginationAndFilter.status);
    console.log(this.paginationAndFilter.status);
    this.getDrivers();
  }

  openEditModal(index:number){
    let editData=JSON.parse(JSON.stringify(this.drivers[index]));
    console.log(editData);
    this.editModal.showModal(editData,2);
  }

  openDeleteModal(driver:any){
    this.deleteModal.showModal(driver,2);
  }

  updated(e){
    console.log("updated");
    this.getDrivers();
  }

  deleted(e){
    console.log("deleted");
    this.getDrivers();
  }
}