import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DriverRoutingModule } from './driver-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';

import { DriverListComponent } from './components/driver-list/driver-list.component';
import { DriverDetailComponent } from './components/driver-detail/driver-detail.component';
import { ConfirmModalComponent } from './components/modals/confirm-modal/confirm-modal.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [DriverListComponent, DriverDetailComponent, ConfirmModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    DriverRoutingModule,
    ModalModule.forRoot()
  ]
})
export class DriverModule { }
