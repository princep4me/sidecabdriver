import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { debounceTime } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  apiUrl = environment.apiUrl ;

  constructor(private http: HttpClient) { }

  getDrivers(paginationAndFilterData) {
    return this.http.post(this.apiUrl + 'drivers', paginationAndFilterData).pipe(debounceTime(2000));
  }

  getDriver(driverId) {
    return this.http.get(this.apiUrl + 'drivers/' + driverId);
  }

  deleteDriver(driverId) {
    return this.http.delete(this.apiUrl + 'drivers/' + driverId);
  }

  updateDriverDetails(driverId:any, data: any) {
    return this.http.put(this.apiUrl + 'drivers/' + driverId, data);
  }
}
