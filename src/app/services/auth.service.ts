import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl = environment.apiUrl ;

  constructor(private http: HttpClient) { }

  login(credentials) {
    return this.http.post( this.apiUrl + 'auth/login', credentials);
  }

  isLoggedIn() {
    const token = localStorage.getItem('authToken');
    if (token) {
      return true;
    } else {
      return false;
    }
  }
}
