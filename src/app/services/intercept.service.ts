import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, catchError, tap} from 'rxjs/operators';
// import { LoaderService } from './loader.service';


@Injectable()// {providedIn: 'root'}

export class InterceptService  implements HttpInterceptor {
    constructor() { }

    // intercept request and add token
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // this.showLoader();
        console.log(localStorage.getItem('authToken'));
        if (localStorage.getItem('authToken')) {
            console.log('1');
            const modified = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + localStorage.getItem('authToken')
                }
            });
            console.log(modified);
            return next.handle(modified).pipe(tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('response recieved');
                    // this.onEnd();
                }
            }));
        } else {
            console.log('2');
            return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('response recieved');
                    // this.onEnd();
                }
            }));
        }
    // modify request
    }

    // private onEnd(): void {
    //     this.hideLoader();
    // }

    // private showLoader(): void {
    //     this.loaderService.show();
    // }

    // private hideLoader(): void {
    //     this.loaderService.hide();
    // }
}
