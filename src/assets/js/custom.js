$(document).ready(function()
{

$('.main-menu-wrapper li a').on("click", function(e)
{
	$(this).next('ul').toggle('slow');
    e.stopPropagation();
    e.preventDefault();
});


  /*sidebar collapse*/
  
$(window).resize(function(){
  if ($(window).width() < 960) 
  {
    $('.page-sidebar').addClass('collapseit');
    $('.page-topbar').addClass('sidebar_shift');
    $('#main-content').addClass('sidebar_shift');
  }
  else 
  {
    $('.page-sidebar').removeClass('collapseit');
    $('.page-topbar').removeClass('sidebar_shift');
    $('#main-content').removeClass('sidebar_shift');
  }
});	

const ps = new PerfectScrollbar('.fixedscroll', {
  wheelSpeed: 2,
  wheelPropagation: true,
  minScrollbarLength: 10
});

$('.page-topbar .sidebar_toggle').on('click', function() {
  var topbar = $(".page-topbar");
  var mainarea = $("#main-content");
  var menuarea = $(".page-sidebar");

  if (menuarea.hasClass("collapseit") || menuarea.hasClass("chat_shift")) {
      menuarea.addClass("expandit").removeClass("collapseit").removeClass("chat_shift");
      topbar.removeClass("sidebar_shift").removeClass("chat_shift");
      mainarea.removeClass("sidebar_shift").removeClass("chat_shift");
  } else {
      menuarea.addClass("collapseit").removeClass("expandit").removeClass("chat_shift");
      topbar.addClass("sidebar_shift").removeClass("chat_shift");
      mainarea.addClass("sidebar_shift").removeClass("chat_shift");
  }
});



});